package com.florentin.galaxyforecastweather.bean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Configuration
public class PropertiesBean {
    @Bean
    public Properties galaxyProperties() {
        Properties prop = new Properties();
        InputStream input = null;

        try {
            input = new ClassPathResource("galaxy.properties").getInputStream();
            prop.load(input);

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return prop;
    }
}
