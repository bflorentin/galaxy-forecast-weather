package com.florentin.galaxyforecastweather;

import com.florentin.galaxyforecastweather.service.PlanetService;
import com.florentin.galaxyforecastweather.service.WeatherForecastService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    PlanetService planetService;
    @Autowired
    WeatherForecastService weatherForecastService;

    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        planetService.createPlanetsFromResources();
        weatherForecastService.generateWeatherForecast();
    }
}
