package com.florentin.galaxyforecastweather.planet.controller;

import com.florentin.galaxyforecastweather.service.dto.PlanetDTO;

import java.util.List;

public interface PlanetController {
    public List<PlanetDTO> getPlanets();
}
