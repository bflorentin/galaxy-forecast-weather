package com.florentin.galaxyforecastweather.planet.controller.impl;

import com.florentin.galaxyforecastweather.service.PlanetService;
import com.florentin.galaxyforecastweather.planet.controller.PlanetController;
import com.florentin.galaxyforecastweather.service.dto.PlanetDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/galaxyforecastweather/planet")
public class PlanetControllerImpl implements PlanetController{
    @Autowired
    private PlanetService planetService;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public List<PlanetDTO> getPlanets() {
        return planetService.getPlanets();
    }
}
