package com.florentin.galaxyforecastweather;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GalaxyForecastWeatherApiApplication {
	public static void main(String[] args) {
		SpringApplication.run(GalaxyForecastWeatherApiApplication.class, args);
	}
}
