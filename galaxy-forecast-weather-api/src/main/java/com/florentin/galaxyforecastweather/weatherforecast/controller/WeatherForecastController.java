package com.florentin.galaxyforecastweather.weatherforecast.controller;

import com.florentin.galaxyforecastweather.service.dto.DayWeatherForecastDTO;
import com.florentin.galaxyforecastweather.service.dto.WeatherForecastDTO;
import com.florentin.galaxyforecastweather.service.dto.WeatherForecastPeriodDTO;

import java.util.List;

public interface WeatherForecastController {
    List<WeatherForecastDTO> getWeatherForecasts();
    List<WeatherForecastPeriodDTO> getWeatherForecastsByPeriod(Integer firstDay, Integer lastDay);
    DayWeatherForecastDTO getWeatherForecast(Integer day);
}
