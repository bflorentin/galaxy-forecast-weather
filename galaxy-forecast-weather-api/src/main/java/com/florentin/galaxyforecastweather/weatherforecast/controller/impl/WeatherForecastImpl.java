package com.florentin.galaxyforecastweather.weatherforecast.controller.impl;

import com.florentin.galaxyforecastweather.service.WeatherForecastService;
import com.florentin.galaxyforecastweather.service.dto.DayWeatherForecastDTO;
import com.florentin.galaxyforecastweather.service.dto.WeatherForecastDTO;
import com.florentin.galaxyforecastweather.service.dto.WeatherForecastPeriodDTO;
import com.florentin.galaxyforecastweather.weatherforecast.controller.WeatherForecastController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/galaxyforecastweather/weatherForecast")
public class WeatherForecastImpl implements WeatherForecastController{
    @Autowired
    private WeatherForecastService weatherForecastService;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public List<WeatherForecastDTO> getWeatherForecasts() {
        return weatherForecastService.getWeatherForecasts();
    }

    @RequestMapping(path = "/{firstDay}/{lastDay}", method = RequestMethod.GET)
    public List<WeatherForecastPeriodDTO> getWeatherForecastsByPeriod(@PathVariable("firstDay")Integer firstDay, @PathVariable("lastDay")Integer lastDay) {
        return weatherForecastService.getWeatherForecastsByPerdiod(firstDay, lastDay);
    }

    @RequestMapping(path = "/{day}", method = RequestMethod.GET)
    public DayWeatherForecastDTO getWeatherForecast(@PathVariable("day") Integer day) {
        return weatherForecastService.getWeatherForecast(day);
    }
}
