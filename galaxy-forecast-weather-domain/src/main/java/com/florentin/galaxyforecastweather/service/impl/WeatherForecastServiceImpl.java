package com.florentin.galaxyforecastweather.service.impl;

import com.florentin.galaxyforecastweather.dao.PlanetDAO;
import com.florentin.galaxyforecastweather.dao.WeatherForecastDAO;
import com.florentin.galaxyforecastweather.model.Planet;
import com.florentin.galaxyforecastweather.model.PlanetWeatherForecast;
import com.florentin.galaxyforecastweather.model.Weather;
import com.florentin.galaxyforecastweather.model.WeatherForecast;
import com.florentin.galaxyforecastweather.service.WeatherConditionService;
import com.florentin.galaxyforecastweather.service.WeatherForecastService;
import com.florentin.galaxyforecastweather.service.dto.DayWeatherForecastDTO;
import com.florentin.galaxyforecastweather.service.dto.WeatherDTO;
import com.florentin.galaxyforecastweather.service.dto.WeatherForecastDTO;
import com.florentin.galaxyforecastweather.service.dto.WeatherForecastPeriodDTO;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class WeatherForecastServiceImpl implements WeatherForecastService{
    @Autowired
    private WeatherConditionService weatherConditionService;
    @Autowired
    private WeatherForecastDAO weatherForecastDAO;
    @Autowired
    private PlanetDAO planetDAO;
    @Autowired
    private ModelMapper modelMapper;

    public List<WeatherForecastDTO> getWeatherForecasts() {
        Type destinationType = new TypeToken<List<WeatherForecastDTO>>() {}.getType();

        List<WeatherForecast> weatherForecasts = weatherForecastDAO.getWeatherForecasts(true);

        return modelMapper.map(weatherForecasts, destinationType);
    }

    public List<WeatherForecastPeriodDTO> getWeatherForecastsByPerdiod(Integer firstDay, Integer lastDay) {
        List<WeatherForecastPeriodDTO> result = new ArrayList<>();

        // Validacion fea para obtener un maximo de 10años
        // TODO: Mejorar esto
        if(firstDay<=0 || lastDay<=0 || firstDay>lastDay || lastDay > 3650)
            return result;


        List<WeatherForecast> totalPeriod = getWeatherForecastsPeriod(firstDay, lastDay);
        for (Weather weather : Weather.values()){
            List<WeatherForecast> listWeather = totalPeriod.stream().filter(x -> weather.equals(x.getWeather())).collect(Collectors.toList());

            WeatherForecastPeriodDTO weatherForecastPeriodDTO = new WeatherForecastPeriodDTO();
            weatherForecastPeriodDTO.setQuantity(listWeather.size());
            weatherForecastPeriodDTO.setWeather( modelMapper.map(weather, WeatherDTO.class));

            result.add(weatherForecastPeriodDTO);
        }

        return result;
    }

    //El maximo de variables climaticas se dan en 360 dias
    //si se solicita el día 361 es lo mismo que el día 1
    public DayWeatherForecastDTO getWeatherForecast(Integer day){
        Integer realDay;
        if(day<=0)
            return null;

        realDay = getRealDay(day);

        WeatherForecast weatherForecast = weatherForecastDAO.getWeatherForecast(realDay);
        DayWeatherForecastDTO dayWeatherForecastDTO = modelMapper.map(weatherForecast, DayWeatherForecastDTO.class);
        dayWeatherForecastDTO.setDay(day);

        return dayWeatherForecastDTO;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void generateWeatherForecast() {
        List<Planet> planets = planetDAO.getPlanets();
        List<WeatherForecast> weatherForecasts = new ArrayList<>();
        //El maximo de combinaciones se registra en 360 días.
        for (Integer i = 1; i<=360; i++){
            WeatherForecast weatherForecast = getWeatherForecastPosition(planets, i);
            weatherForecasts.add(weatherForecast);
        }

        weatherConditionService.fillWeather(weatherForecasts);

        weatherForecastDAO.addWeatherForecasts(weatherForecasts);
    }
    //region Private
    private WeatherForecast getWeatherForecastPosition(List<Planet> planets, Integer day){
        WeatherForecast weatherForecast = new WeatherForecast();
        weatherForecast.setDay(day);
        List<PlanetWeatherForecast> planetWeatherForecasts = new ArrayList<>();
        for (Planet planet : planets) {
            Double position = planet.getInitialPosition() - (planet.getSpeed() * day);
            PlanetWeatherForecast planetWeatherForecast = new PlanetWeatherForecast();
            planetWeatherForecast.setPlanet(planet);
            planetWeatherForecast.setPosition(position);
            planetWeatherForecast.setWeatherForecast(weatherForecast);

            planetWeatherForecasts.add(planetWeatherForecast);
        }
        weatherForecast.setPlanetWeatherForecasts(planetWeatherForecasts);

        return weatherForecast;
    }

    private Integer getRealDay(Integer day){
        Integer realDay;

        Double a = day / 360D;
        Integer b = (int) Math.floor(a);

        realDay = (int) Math.round((a-b) *360D);

        return realDay == 0 ? 360 : realDay;
    }

    private List<WeatherForecast> getWeatherForecastsPeriod(Integer firstDay, Integer lastDay) {
        List<WeatherForecast> weatherForecasts = weatherForecastDAO.getWeatherForecasts(false);
        Map<Integer, WeatherForecast> map = weatherForecasts.stream().collect(Collectors.toMap(WeatherForecast::getDay, weatherForecast -> weatherForecast));

        List<WeatherForecast> totalPeriod = new ArrayList<>();

        // TODO: Ver si se puede mejorar esto
        for (Integer day = firstDay; day<=lastDay; day++){
            Integer realDay = getRealDay(day);
            totalPeriod.add(map.get(realDay));
        }

        return totalPeriod;
    }
    //endregion
}
