package com.florentin.galaxyforecastweather.service;

import com.florentin.galaxyforecastweather.model.WeatherForecast;

import java.util.List;

public interface WeatherConditionService {
    void fillWeather(List<WeatherForecast> weatherForecasts);
}
