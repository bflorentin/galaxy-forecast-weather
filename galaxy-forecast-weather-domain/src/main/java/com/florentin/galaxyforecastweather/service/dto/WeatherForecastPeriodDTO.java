package com.florentin.galaxyforecastweather.service.dto;

import java.io.Serializable;
import java.util.List;

public class WeatherForecastPeriodDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private WeatherDTO weather;
    private Integer quantity;
    private List<Integer> days;

    public WeatherForecastPeriodDTO() {
    }

    public WeatherDTO getWeather() {
        return weather;
    }

    public void setWeather(WeatherDTO weather) {
        this.weather = weather;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public List<Integer> getDays() {
        return days;
    }

    public void setDays(List<Integer> days) {
        this.days = days;
    }
}
