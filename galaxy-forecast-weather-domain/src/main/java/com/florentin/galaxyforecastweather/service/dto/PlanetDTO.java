package com.florentin.galaxyforecastweather.service.dto;

import java.io.Serializable;

public class PlanetDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private String name;
    private Double initialPosition;
    private Double speed;
    private Double orbitRadius;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getInitialPosition() {
        return initialPosition;
    }

    public void setInitialPosition(Double initialPosition) {
        this.initialPosition = initialPosition;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Double getOrbitRadius() {
        return orbitRadius;
    }

    public void setOrbitRadius(Double orbitRadius) {
        this.orbitRadius = orbitRadius;
    }
}
