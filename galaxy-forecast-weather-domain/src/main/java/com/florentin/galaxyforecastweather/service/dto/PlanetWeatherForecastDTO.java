package com.florentin.galaxyforecastweather.service.dto;

import java.io.Serializable;

public class PlanetWeatherForecastDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer planetId;
    private String planetName;
    private Double planetOrbitRadius;
    private Double position;

    public Integer getPlanetId() {
        return planetId;
    }

    public void setPlanetId(Integer planetId) {
        this.planetId = planetId;
    }

    public String getPlanetName() {
        return planetName;
    }

    public void setPlanetName(String planetName) {
        this.planetName = planetName;
    }

    public Double getPlanetOrbitRadius() {
        return planetOrbitRadius;
    }

    public void setPlanetOrbitRadius(Double planetOrbitRadius) {
        this.planetOrbitRadius = planetOrbitRadius;
    }

    public Double getPosition() {
        return position;
    }

    public void setPosition(Double position) {
        this.position = position;
    }
}
