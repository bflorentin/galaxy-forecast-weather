package com.florentin.galaxyforecastweather.service.dto;

import java.io.Serializable;

public class DayWeatherForecastDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer day;
    private WeatherDTO weather;

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public WeatherDTO getWeather() {
        return weather;
    }

    public void setWeather(WeatherDTO weather) {
        this.weather = weather;
    }
}
