package com.florentin.galaxyforecastweather.service.dto;

public enum WeatherDTO {
    RAIN("RAIN"),
    HEAVY_RAIN("HEAVY_RAIN"),
    DROUGHT("DROUGHT"),
    OPTIMUM("OPTIMUM"),
    UNKNOWN("UNKNOWN");

    private String code;

    private WeatherDTO(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return this.code;
    }
}
