package com.florentin.galaxyforecastweather.service;

import com.florentin.galaxyforecastweather.service.dto.PlanetDTO;

import java.util.List;

public interface PlanetService {
    void createPlanetsFromResources();
    List<PlanetDTO> getPlanets();
    void addPlanet(PlanetDTO planetDTO);
    void addPlanets(List<PlanetDTO> planetsDTO);
}
