package com.florentin.galaxyforecastweather.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.florentin.galaxyforecastweather.dao.PlanetDAO;
import com.florentin.galaxyforecastweather.model.Planet;
import com.florentin.galaxyforecastweather.service.PlanetService;
import com.florentin.galaxyforecastweather.service.dto.PlanetDTO;
import org.apache.commons.io.IOUtils;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.List;

@Service
public class PlanetServiceImpl implements PlanetService{
    @Autowired
    private PlanetDAO planetDAO;
    @Autowired
    private ModelMapper modelMapper;

    public void createPlanetsFromResources(){
        final String ENCODING = "UTF-8";
        List<PlanetDTO> planetsDTO = null;
        try {
            ObjectMapper objectMapper = new ObjectMapper();

            InputStream inputStream = new ClassPathResource("planets.json").getInputStream();
            String result = IOUtils.toString(inputStream, ENCODING);

            planetsDTO = objectMapper.readValue(result, new TypeReference<List<PlanetDTO>>(){});

            System.out.print(result);
        } catch (IOException e) {
            e.printStackTrace();
        }

        addPlanets(planetsDTO);
    }

    public List<PlanetDTO> getPlanets() {
        Type destinationType = new TypeToken<List<PlanetDTO>>() {}.getType();

        List<Planet> planets = planetDAO.getPlanets();

        return modelMapper.map(planets, destinationType);
    }

    public void addPlanet(PlanetDTO planetDTO) {
        Planet planet = modelMapper.map(planetDTO, Planet.class);
        planetDAO.addPlanet(planet);
    }

    public void addPlanets(List<PlanetDTO> planetsDTO) {
        Type destinationType = new TypeToken<List<Planet>>() {}.getType();

        List<Planet> planets = modelMapper.map(planetsDTO, destinationType);
        planetDAO.addPlanets(planets);
    }
}
