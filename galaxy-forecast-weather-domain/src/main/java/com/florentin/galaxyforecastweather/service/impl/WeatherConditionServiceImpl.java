package com.florentin.galaxyforecastweather.service.impl;

import com.florentin.galaxyforecastweather.helper.MathHelper;
import com.florentin.galaxyforecastweather.helper.dto.PolarCoordinatesDTO;
import com.florentin.galaxyforecastweather.model.PlanetWeatherForecast;
import com.florentin.galaxyforecastweather.model.Weather;
import com.florentin.galaxyforecastweather.model.WeatherForecast;
import com.florentin.galaxyforecastweather.service.WeatherConditionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class WeatherConditionServiceImpl implements WeatherConditionService{

    @Autowired
    @Qualifier("galaxyProperties")
    private Properties galaxyProperties;

    public void fillWeather(List<WeatherForecast> weatherForecasts){
        Map<Integer, Double> perimeters = new Hashtable<>();

        for (WeatherForecast weatherForecast : weatherForecasts) {
            Weather weather = Weather.UNKNOWN;

            if(isAlignedWithSun(weatherForecast))
                weather = Weather.DROUGHT;
            else if(isAlignedWithoutSun(weatherForecast))
                weather = Weather.OPTIMUM;
            else if (sunIsIntoPerimeter(weatherForecast, perimeters))
                weather=Weather.RAIN;

            weatherForecast.setWeather(weather);
        }

        checkHeavyRain(weatherForecasts, perimeters);
    }

    private Boolean isAlignedWithoutSun(WeatherForecast weatherForecast){
        final Double PLANETS_ALIGNMENT_TOLERANCE = Double.parseDouble(galaxyProperties.getProperty("planetsAlignmentTolerance"));

        Boolean result = true;

        Double firstSlope = null;
        Integer size = weatherForecast.getPlanetWeatherForecasts().size();
        PlanetWeatherForecast planetWeatherForecast1 = weatherForecast.getPlanetWeatherForecasts().get(size-1);
        for (Integer i = size-2; i>=0; i--){
            PlanetWeatherForecast planetWeatherForecast2 = weatherForecast.getPlanetWeatherForecasts().get(i);

            PolarCoordinatesDTO planet1 = new PolarCoordinatesDTO(planetWeatherForecast1.getPlanet().getOrbitRadius(), planetWeatherForecast1.getPosition());
            PolarCoordinatesDTO planet2 = new PolarCoordinatesDTO(planetWeatherForecast2.getPlanet().getOrbitRadius(), planetWeatherForecast2.getPosition());

            Double slope = MathHelper.getSlope(planet1, planet2);
            if(firstSlope == null)
                firstSlope = slope;
            else if(Math.abs(MathHelper.round(firstSlope, 2)-MathHelper.round(slope, 2)) > PLANETS_ALIGNMENT_TOLERANCE){
                result = false;
                break;
            }
        }

        return result;
    }

    private Boolean isAlignedWithSun(WeatherForecast weatherForecast){
        final Double SUN_ALIGNMENT_TOLERANCE = Double.parseDouble(galaxyProperties.getProperty("sunAlignmentTolerance"));

        Boolean result = true;

        Double firstSlope = null;
        for (Integer i = 0; i<weatherForecast.getPlanetWeatherForecasts().size(); i++){
            PlanetWeatherForecast planetWeatherForecast = weatherForecast.getPlanetWeatherForecasts().get(i);
            PolarCoordinatesDTO sun = new PolarCoordinatesDTO(0D,0D);
            PolarCoordinatesDTO planet = new PolarCoordinatesDTO(planetWeatherForecast.getPlanet().getOrbitRadius(), planetWeatherForecast.getPosition());

            Double slope = MathHelper.getSlope(sun, planet);
            if(firstSlope == null)
                firstSlope = slope;
            else if(Math.abs(firstSlope-slope) > SUN_ALIGNMENT_TOLERANCE){
                result = false;
                break;
            }
        }

        return result;
    }

    private Boolean sunIsIntoPerimeter(WeatherForecast weatherForecast, Map<Integer, Double> perimeters){
        PolarCoordinatesDTO sun = new PolarCoordinatesDTO(0D,0D);

        List<PolarCoordinatesDTO> polygonVertices = new ArrayList<>();
        for (PlanetWeatherForecast planetWeatherForecast : weatherForecast.getPlanetWeatherForecasts()) {
            PolarCoordinatesDTO planet = new PolarCoordinatesDTO(planetWeatherForecast.getPlanet().getOrbitRadius(), planetWeatherForecast.getPosition());

            polygonVertices.add(planet);
        }

        Boolean pointIntoPolygon = MathHelper.pointIntoPolygon(polygonVertices, sun);
        if(pointIntoPolygon){
            Double perimeter = MathHelper.getPolygonPerimeter(polygonVertices);
            perimeters.put(weatherForecast.getDay(), perimeter);
        }

        return pointIntoPolygon;
    }

    private void checkHeavyRain(List<WeatherForecast> weatherForecasts, Map<Integer, Double> perimeters) {
        final Double MAX_PERIMETER_TOLERANCE = Double.parseDouble(galaxyProperties.getProperty("maxPerimeterTolerance"));

        if(perimeters.isEmpty())
            return;

        List<WeatherForecast> rainDays =  weatherForecasts.stream().filter(x -> Weather.RAIN.equals(x.getWeather())).collect(Collectors.toList());

        Double maxPerimeter = getMaxPerimeter(perimeters);
        for (WeatherForecast weatherForecast : rainDays) {
            Double perimeter = perimeters.get(weatherForecast.getDay());
            if(Math.abs(maxPerimeter-perimeter) < MAX_PERIMETER_TOLERANCE){
                weatherForecast.setWeather(Weather.HEAVY_RAIN);
            }
        }
    }

    private Double getMaxPerimeter(Map<Integer, Double> perimeters){
        Double maxPerimeter = 0D;
        if(!perimeters.isEmpty()){
            List<Double> values = new ArrayList<>(perimeters.values());
            values.sort(Collections.reverseOrder());

            maxPerimeter = values.iterator().next();
        }

        return maxPerimeter;
    }
}
