package com.florentin.galaxyforecastweather.service;

import com.florentin.galaxyforecastweather.service.dto.DayWeatherForecastDTO;
import com.florentin.galaxyforecastweather.service.dto.WeatherForecastDTO;
import com.florentin.galaxyforecastweather.service.dto.WeatherForecastPeriodDTO;

import java.util.List;

public interface WeatherForecastService {
    List<WeatherForecastDTO> getWeatherForecasts();
    List<WeatherForecastPeriodDTO> getWeatherForecastsByPerdiod(Integer firstDay, Integer LastDay);
    DayWeatherForecastDTO getWeatherForecast(Integer day);

    void generateWeatherForecast();
}
