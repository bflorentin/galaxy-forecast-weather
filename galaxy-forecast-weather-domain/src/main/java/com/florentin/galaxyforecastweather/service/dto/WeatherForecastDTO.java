package com.florentin.galaxyforecastweather.service.dto;

import java.io.Serializable;
import java.util.List;

public class WeatherForecastDTO extends DayWeatherForecastDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<PlanetWeatherForecastDTO> planetWeatherForecasts;

    public List<PlanetWeatherForecastDTO> getPlanetWeatherForecasts() {
        return planetWeatherForecasts;
    }

    public void setPlanetWeatherForecast(List<PlanetWeatherForecastDTO> planetWeatherForecasts) {
        this.planetWeatherForecasts = planetWeatherForecasts;
    }
}
