package com.florentin.galaxyforecastweather.helper.dto;

public class PolarCoordinatesDTO {
    private Double radius;
    private Double angle;

    public PolarCoordinatesDTO(Double radius, Double angle){
        this.radius = radius;
        this.angle = angle;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public Double getAngle() {
        return angle;
    }

    public void setAngle(Double angle) {
        this.angle = angle;
    }
}
