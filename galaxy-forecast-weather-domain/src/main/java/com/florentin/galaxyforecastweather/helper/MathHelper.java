package com.florentin.galaxyforecastweather.helper;

import com.florentin.galaxyforecastweather.helper.dto.CartesianCoordinatesDTO;
import com.florentin.galaxyforecastweather.helper.dto.PolarCoordinatesDTO;

import java.util.ArrayList;
import java.util.List;

public class MathHelper {
    public static PolarCoordinatesDTO convertCartesianToPolarCoordinates(Double x, Double y){
        Double radius = Math.hypot(x, y);
        Double angleRad = Math.atan2(y, x);
        Double angle = Math.toDegrees(angleRad);

        return new PolarCoordinatesDTO(radius, angle);
    }

    public static CartesianCoordinatesDTO convertPolarToCartesianCoordinates(Double radius, Double angle){
        Double angleRad = Math.toRadians(angle);

        Double cos = Math.cos(angleRad);
        Double sin = Math.sin(angleRad);
        Double cosRound = round(cos, 8);
        Double sinRound = round(sin, 8);

        Double x = radius * cosRound;
        Double y = radius * sinRound;

        return new CartesianCoordinatesDTO(x, y);
    }

    public static Double getSlope(CartesianCoordinatesDTO point1, CartesianCoordinatesDTO point2) {
        Double x = point2.getX() - point1.getX();
        Double y = point2.getY() - point1.getY();

        if(x.equals(0D))
            return Double.POSITIVE_INFINITY;
        return y/x;
    }

    public static Double getSlope(PolarCoordinatesDTO point1, PolarCoordinatesDTO point2) {
        CartesianCoordinatesDTO cartesianPoint1 = convertPolarToCartesianCoordinates(point1.getRadius(), point1.getAngle());
        CartesianCoordinatesDTO cartesianPoint2 = convertPolarToCartesianCoordinates(point2.getRadius(), point2.getAngle());

        return getSlope(cartesianPoint1, cartesianPoint2);
    }

    public static Double getTriangleOrientation(CartesianCoordinatesDTO point1, CartesianCoordinatesDTO point2, CartesianCoordinatesDTO point3){
        return (point1.getX() - point3.getX()) * (point2.getY() - point3.getY()) - (point1.getY() - point3.getX()) * (point2.getX() - point3.getX());
    }

    public static Double getTriangleOrientation(PolarCoordinatesDTO point1, PolarCoordinatesDTO point2, PolarCoordinatesDTO point3){
        CartesianCoordinatesDTO cartesianPoint1 = convertPolarToCartesianCoordinates(point1.getRadius(), point1.getAngle());
        CartesianCoordinatesDTO cartesianPoint2 = convertPolarToCartesianCoordinates(point2.getRadius(), point2.getAngle());
        CartesianCoordinatesDTO cartesianPoint3 = convertPolarToCartesianCoordinates(point3.getRadius(), point3.getAngle());

        return getTriangleOrientation(cartesianPoint1, cartesianPoint2, cartesianPoint3);
    }

    public static Boolean pointIntoPolygon(List<CartesianCoordinatesDTO> polygonVertices, CartesianCoordinatesDTO point){
        Integer count = 0;
        CartesianCoordinatesDTO vertex1 = polygonVertices.get(0);
        for (int i = 1; i <= polygonVertices.size(); i++)
        {
            CartesianCoordinatesDTO vertex2 = polygonVertices.get(i%polygonVertices.size());
            if (point.getY() > Math.min(vertex1.getY(), vertex2.getY()))
            {
                if (point.getY() <= Math.max(vertex1.getY(), vertex2.getY()))
                {
                    if (point.getX() <= Math.max(vertex1.getX(), vertex2.getX()))
                    {
                        if (!vertex1.getY().equals(vertex2.getY())) {
                            Double xInters = (point.getY() - vertex1.getY()) * (vertex2.getX() - vertex1.getX()) / (vertex2.getY() - vertex1.getY()) + vertex1.getX();
                            if (vertex1.getX().equals(vertex2.getX()) || point.getX() <= xInters)
                                count++;
                        }
                    }
                }
            }
            vertex1 = vertex2;
        }
        return count % 2 != 0;
    }

    public static Boolean pointIntoPolygon(List<PolarCoordinatesDTO> polygonVertices, PolarCoordinatesDTO point){
        List<CartesianCoordinatesDTO> cartesianPolygonVertices = new ArrayList<CartesianCoordinatesDTO>();
        for (PolarCoordinatesDTO polarVertex : polygonVertices) {
            CartesianCoordinatesDTO cartesianVertex = convertPolarToCartesianCoordinates(polarVertex.getRadius(),polarVertex.getAngle());

            cartesianPolygonVertices.add(cartesianVertex);
        }
        CartesianCoordinatesDTO cartesianPoint = convertPolarToCartesianCoordinates(point.getRadius(),point.getAngle());

        return pointIntoPolygon(cartesianPolygonVertices,cartesianPoint);
    }

    public static Double getPolygonPerimeter(List<PolarCoordinatesDTO> polygonVertices){
        List<CartesianCoordinatesDTO> cartesianPolygonVertices = new ArrayList<>();
        for (PolarCoordinatesDTO polarVertex : polygonVertices) {
            CartesianCoordinatesDTO cartesianVertex = convertPolarToCartesianCoordinates(polarVertex.getRadius(),polarVertex.getAngle());

            cartesianPolygonVertices.add(cartesianVertex);
        }

        return calculatePolygonPerimeter(cartesianPolygonVertices);
    }

    private static Double calculatePolygonPerimeter(List<CartesianCoordinatesDTO> polygonVertices){
        Integer size = polygonVertices.size();
        Double perimeter = 0D;
        for (Integer i = 0; i<size; i++){
            Integer indexVertex2 = i+1;
            if(i==size-1)
                indexVertex2=0;
            CartesianCoordinatesDTO vertex1 = polygonVertices.get(i);
            CartesianCoordinatesDTO vertex2 = polygonVertices.get(indexVertex2);

            Double distance = Math.hypot(vertex2.getX()-vertex1.getX(), vertex2.getY()-vertex1.getY());
            perimeter += distance;
        }

        return perimeter;
    }

    public static Double round(Double value, Integer decimals){
        decimals++;
        String decimalsStr = String.format("%1$-" + decimals + "s", "1").replace(' ', '0');

        Double dec = Double.parseDouble(decimalsStr);

        return Math.round(value * dec) / dec;
    }
}
