package com.florentin.galaxyforecastweather.dao.impl;

import com.florentin.galaxyforecastweather.dao.WeatherForecastDAO;
import com.florentin.galaxyforecastweather.model.WeatherForecast;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Transactional
@Repository
public class WeatherForecastDAOImpl implements WeatherForecastDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    public List<WeatherForecast> getWeatherForecasts(Boolean deep) {
        String hql = "FROM WeatherForecast";
        if (deep)
            hql += " w JOIN FETCH w.planetWeatherForecasts p JOIN FETCH p.planet";

        return (List<WeatherForecast>) entityManager.createQuery(hql).getResultList();
    }

    @SuppressWarnings("unchecked")
    public WeatherForecast getWeatherForecast(Integer day) {
        String hql = "FROM WeatherForecast WHERE day = :day";
        Query query = entityManager.createQuery(hql);
        query.setParameter("day", day);

        List<WeatherForecast> queryResult = query.getResultList();

        return queryResult.iterator().next();
    }

    public void addWeatherForecast(WeatherForecast weatherForecast) {
        entityManager.persist(weatherForecast);
    }

    public void addWeatherForecasts(List<WeatherForecast> weatherForecasts) {
        for (WeatherForecast weatherForecast: weatherForecasts) {
            addWeatherForecast(weatherForecast);
        }
    }
}
