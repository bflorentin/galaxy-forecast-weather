package com.florentin.galaxyforecastweather.dao;

import com.florentin.galaxyforecastweather.model.WeatherForecast;

import java.util.List;

public interface WeatherForecastDAO {
    List<WeatherForecast> getWeatherForecasts(Boolean deep);
    WeatherForecast getWeatherForecast(Integer day);
    void addWeatherForecast(WeatherForecast weatherForecast);
    void addWeatherForecasts(List<WeatherForecast> weatherForecasts);
}
