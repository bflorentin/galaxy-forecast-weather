package com.florentin.galaxyforecastweather.dao.impl;

import com.florentin.galaxyforecastweather.dao.PlanetDAO;
import com.florentin.galaxyforecastweather.model.Planet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Transactional
@Repository
public class PlanetDAOImpl implements PlanetDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    public List<Planet> getPlanets() {
        String hql = "FROM Planet";
        return (List<Planet>) entityManager.createQuery(hql).getResultList();
    }

    public void addPlanet(Planet planet) {
        entityManager.persist(planet);
    }
    public void addPlanets(List<Planet> planets) {
        for (Planet planet: planets) {
            addPlanet(planet);
        }
    }
}
