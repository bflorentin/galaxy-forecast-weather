package com.florentin.galaxyforecastweather.dao;

import com.florentin.galaxyforecastweather.model.Planet;

import java.util.List;

public interface PlanetDAO {
    List<Planet> getPlanets();
    void addPlanet(Planet planet);
    void addPlanets(List<Planet> planets);
}
