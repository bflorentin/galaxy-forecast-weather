package com.florentin.galaxyforecastweather.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "planets")
public class Planet implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "initialPosition")
    private Double initialPosition;
    @Column(name = "speed")
    private Double speed;
    @Column(name = "orbitRadius")
    private Double orbitRadius;
    @OneToMany(mappedBy = "weatherForecast", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<PlanetWeatherForecast> planetWeatherForecasts;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getInitialPosition() {
        return initialPosition;
    }

    public void setInitialPosition(Double initialPosition) {
        this.initialPosition = initialPosition;
    }

    /**
     * Velocidad Angular
     */
    public Double getSpeed() {
        return speed;
    }

    /**
     * Velocidad Angular
     */
    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Double getOrbitRadius() {
        return orbitRadius;
    }

    public void setOrbitRadius(Double orbitRadius) {
        this.orbitRadius = orbitRadius;
    }

    public List<PlanetWeatherForecast> getPlanetWeatherForecasts() {
        return planetWeatherForecasts;
    }

    public void setPlanetWeatherForecasts(List<PlanetWeatherForecast> planetWeatherForecasts) {
        this.planetWeatherForecasts = planetWeatherForecasts;
    }
}
