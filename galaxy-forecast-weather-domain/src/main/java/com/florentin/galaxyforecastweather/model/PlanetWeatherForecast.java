package com.florentin.galaxyforecastweather.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "planetweatherforecasts")
public class PlanetWeatherForecast implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "planetId")
    private Planet planet;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "weatherForecastId")
    private WeatherForecast weatherForecast;
    @Column(name = "position")
    private Double position;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Planet getPlanet() {
        return planet;
    }

    public void setPlanet(Planet planet) {
        this.planet = planet;
    }

    public WeatherForecast getWeatherForecast() {
        return weatherForecast;
    }

    public void setWeatherForecast(WeatherForecast weatherForecast) {
        this.weatherForecast = weatherForecast;
    }

    public Double getPosition() {
        return position;
    }

    public void setPosition(Double position) {
        this.position = position;
    }
}
