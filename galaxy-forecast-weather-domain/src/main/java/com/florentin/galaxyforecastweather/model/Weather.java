package com.florentin.galaxyforecastweather.model;

public enum Weather {
    RAIN("RAIN"),
    HEAVY_RAIN("HEAVY_RAIN"),
    DROUGHT("DROUGHT"),
    OPTIMUM("OPTIMUM"),
    UNKNOWN("UNKNOWN");

    private String code;

    private Weather(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return this.code;
    }
}
