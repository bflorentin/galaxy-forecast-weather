package com.florentin.galaxyforecastweather.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "weatherforecasts")
public class WeatherForecast implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(name = "day")
    private Integer day;
    @Column(name = "weather")
    @Enumerated(EnumType.STRING)
    private Weather weather;
    @OneToMany(mappedBy = "weatherForecast", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<PlanetWeatherForecast> planetWeatherForecasts;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    public List<PlanetWeatherForecast> getPlanetWeatherForecasts() {
        return planetWeatherForecasts;
    }

    public void setPlanetWeatherForecasts(List<PlanetWeatherForecast> planetWeatherForecasts) {
        this.planetWeatherForecasts = planetWeatherForecasts;
    }
}
