# GALAXY FORECAST WEATHER #

Es un sistema informático que permite predecir el pronóstico del tiempo de una galaxia.
Esta primera versión funciona correctamente con 3 planetas, también funciona con más pero con un margen de error mayor al calcular los días lluviosos.

## Datos Técnicos ##

* Existe un archivo llamado planets.json para poder inicializar la galaxia con la cantidad de planetas que se desee.
* La posición está expresada en ángulos.
* La velocidad está expresada en ángulos, siendo esta positivo si el movimiento es de sentido horario o negativo si es de movimiento anti-horario.
* Para poder calcular ciertas condiciones climáticas se maneja una tolerancia.
* * Para saber si los planetas están alineados (ya que nunca se alinean al 100%)
* * El área máxima del polígono es un rango (+- 50).


## Configuraciones ##
* sunAlignmentTolerance = Tolerancia de la pendiente entre el sol y los planetas
* planetsAlignmentTolerance = Tolerancia de la pendiente entre el primer planeta y los demas
* maxPerimeterTolerance = Tolerancia +- entre del perimetro maximo

### galaxy.properties ###

```
#!bash

sunAlignmentTolerance=0.1
planetsAlignmentTolerance=0.08
maxPerimeterTolerance=50
```


## Ejecutar Localmente ##
```
#!bash

mvn clean install
```
```
#!bash

java -jar galaxy-forecast-weather-api/target/galaxy-forecast-weather-api-0.0.1-SNAPSHOT.jar
```
* Obtener Lista de planetas
* * http://localhost:8080/galaxyforecastweather/planet
* Lista de todas las variantes climáticas (360 días)
* * http://localhost:8080/galaxyforecastweather/weatherForecast
* Obtener el clima de un día puntual
* * http://localhost:8080/galaxyforecastweather/weatherForecast/{day}
* * http://localhost:8080/galaxyforecastweather/weatherForecast/566
* Obtener climas por periodo
* * http://localhost:8080/galaxyforecastweather/weatherForecast/{firstDay}/{lastDay}
* * http://localhost:8080/galaxyforecastweather/weatherForecast/1/3650

## Versión Productiva ##

* Obtener Lista de planetas
* * http://florentin.com.ar/galaxyforecastweather/planet
* Lista de todas las variantes climáticas (360 días)
* * http://florentin.com.ar/galaxyforecastweather/weatherForecast
* Obtener el clima de un día puntual
* * http://florentin.com.ar/galaxyforecastweather/weatherForecast/{day}
* * http://florentin.com.ar/galaxyforecastweather/weatherForecast/566
* Obtener climas por periodo
* * http://florentin.com.ar/galaxyforecastweather/weatherForecast/{firstDay}/{lastDay}
* * http://florentin.com.ar/galaxyforecastweather/weatherForecast/1/3650

## Herramienta Grafica para poder ver y verificar los climas (3 Planetas) ##
* http://florentin.com.ar/galaxyforecastweatherweb